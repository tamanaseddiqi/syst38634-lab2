import static org.junit.Assert.*;

import org.junit.Test;

import time.Time;


/**
 *
 * @author Tamana Seddiqi
 * Lab#2
 *
 */
public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}


	@Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsException(){
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("THe time provided is not valid");
	}

	@Test
	public void testGetTotalSecondsBoundaryIn(){
		int totalSeconds = Time.getTotalSeconds("00:00:01");
		assertTrue("The time provided does not match the result", totalSeconds == 1);
	}

	@Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut(){
		int totalSeconds = Time.getTotalSeconds("00:00:60");
		fail("THe time provided is not valid");
	}


//	@Test
//	public void testGetTotalMillisecondsRegular(){
//		fail("Invalid number of milliseconds");
//	}

	@Test
	public void testGetTotalMillisecondsRegular(){
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}

//	//Exception case for red phase
//	@Test
//	public void testGetTotalMillisecondsException(){
//		fail("Invalid number of milliseconds");
//	}


	@Test(expected=NumberFormatException.class)
	public void testGetTotalMillisecondsException(){
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
	}

	//for the fail scenario

//	@Test
//	public void testGetTotalMillisecondsBoundaryIn(){
//		fail("Invalid number of seconds");
//	}


	@Test
	public void testGetTotalMillisecondsBoundaryIn(){
		int totalMilliseconds = Time.getTotalMilliseconds("00:00:00:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}

//
//	@Test
//	public void testGetTotalMillisecondsBoundaryOut(){
//		fail("Invalid number of seconds");
//	}

	@Test(expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut(){
		int totalMilliseconds = Time.getTotalMilliseconds("00:00:00:1000");
		fail("Invalid number of milliseconds");
	}

}
